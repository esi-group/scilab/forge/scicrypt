//***********************************************************************
// Vincent COUVERT							
// Florent LEBEAU							
// DIGITEO 2009								
// SciCrypt Toolbox							
// This file is released under the terms of the CeCILL license.		
//***********************************************************************

cur_path = get_absolute_file_path('builder_cpp.sce');

lib_path = cur_path + '../../lib/';

includes_path = cur_path + '../../includes/';

tbx_build_src(..
	['SciCrypt_src'],..
	['cphr.cpp', 'rsa.cpp', 'aes.cpp', 'tdes.cpp', 'serpent.cpp', 'twofish.cpp', 'cast256.cpp', 'blowfish.cpp', 'functions.cpp'],..
	'cpp', ..
	cur_path,..
	' -l' + lib_path + 'crypto++',..
	'',..
	' -I' + includes_path..
	);

clear tbx_build_src;

