//***********************************************************************
// Vincent COUVERT							
// Florent LEBEAU							
// DIGITEO 2009								
// SciCrypt Toolbox							
// This file is released under the terms of the CeCILL license.		
//***********************************************************************

#include "serpent.hpp"


serpent_cbc::serpent_cbc() : rsa(SERPENT, CBC) {}

string serpent_cbc::enc(string plain)
{
	string cipher;
	    
	try
	{	
		CBC_Mode< Serpent >::Encryption e;
		
		cout << e.AlgorithmName() << " encryption" << endl;
		
		e.SetKeyWithIV( key, keylength, iv );

		StringSource( plain, true, 
			new StreamTransformationFilter( e,
				new StringSink( cipher )
			) // StreamTransformationFilter      
		); // StringSource		
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

	return cipher;
}

bool serpent_cbc::encryption(const string& pKey, const string& key, const string& iv, const string& plainF)
{	
	if(loadKeyIV(key, iv))
	{
		return EXIT_FAILURE;
	}			     
	
	//////////////////////////Encoding
	string plain, cipher;
	plain = readF(plainF);
	cipher = enc(plain);
	if(createCphrF(cipher, outFilename))
	{
		cout << "ERROR creating encoded file" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////RSA encoding
	load_pubKey(pKey);
	string cipherKey1 = rsa_enc(getKey());
	if(createF(cipherKey1, "cipherKey"))
	{
		cout << "ERROR creating encoded key file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

string serpent_cbc::dec(string cipher)
{
	string recovered;
	
	try
	{		
		CBC_Mode< Serpent >::Decryption d;
		
		cout << d.AlgorithmName() << " decryption" << endl;
		
		d.SetKeyWithIV( key, keylength, iv );

		StringSource( cipher, true, 
			new StreamTransformationFilter( d,
				new StringSink( recovered )
			) // StreamTransformationFilter
		); // StringSource
	}
    catch( CryptoPP::Exception& e )
    {
        cerr << "Caught Exception..." << endl;
        cerr << e.what() << endl;
        cerr << endl;
    }

    return recovered;
}

bool serpent_cbc::decryption(const string& sKey, const string& cipherKeyF, const string& iv, const string& cipherF)
{	
	if(loadIV(iv))
	{
		cout << "ERROR setting IV" << endl;
		return EXIT_FAILURE;
	}
	//////////////////////////RSA decoding
	load_privKey(sKey);
	string cipherKey = readCphrF(cipherKeyF);
	string recoveredKey = rsa_dec(cipherKey);
	if(setKey(recoveredKey))
	{
		cout << "ERROR loading recovered key" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////Decoding
	string cipher = readCphrF(cipherF);
	string recovered = dec(cipher);
	if(createF(recovered, outFilename))
	{
		cout << "ERROR creating decoded file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

bool serpent_cbc::KeysGenerator()
{
	if(genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	saveKeyIV("key", "iv");
	if(save_pubKey("pKey"))
	{
		return EXIT_FAILURE;
	}
	if(save_privKey("sKey"))
	{
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

serpent_cbc::~serpent_cbc(){}

/***************************************************************************/

serpent_ofb::serpent_ofb() : rsa(SERPENT, OFB) {}

string serpent_ofb::enc(string plain)
{
	string cipher;
	    
	try
	{	
		OFB_Mode< Serpent >::Encryption e;
		
		cout << e.AlgorithmName() << " encryption" << endl;
		
		e.SetKeyWithIV( key, keylength, iv );

		StringSource( plain, true, 
			new StreamTransformationFilter( e,
				new StringSink( cipher )
			) // StreamTransformationFilter      
		); // StringSource		
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

	return cipher;
}

bool serpent_ofb::encryption(const string& pKey, const string& key, const string& iv, const string& plainF)
{
	if(loadKeyIV(key, iv))
	{
		return EXIT_FAILURE;
	}			     
	
	//////////////////////////Encoding
	string plain, cipher;
	plain = readF(plainF);
	cipher = enc(plain);
	if(createCphrF(cipher, outFilename))
	{
		cout << "ERROR creating encoded file" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////RSA encoding
	load_pubKey(pKey);
	string cipherKey1 = rsa_enc(getKey());
	if(createF(cipherKey1, "cipherKey"))
	{
		cout << "ERROR creating encoded key file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

string serpent_ofb::dec(string cipher)
{
	string recovered;
	
	try
	{		
		OFB_Mode< Serpent >::Decryption d;
		
		cout << d.AlgorithmName() << " decryption" << endl;
		
		d.SetKeyWithIV( key, keylength, iv );

		StringSource( cipher, true, 
			new StreamTransformationFilter( d,
				new StringSink( recovered )
			) // StreamTransformationFilter
		); // StringSource
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

    return recovered;
}

bool serpent_ofb::decryption(const string& sKey, const string& cipherKeyF, const string& iv, const string& cipherF)
{
	if(loadIV(iv))
	{
		cout << "ERROR setting IV" << endl;
		return EXIT_FAILURE;
	}
	//////////////////////////RSA decoding
	load_privKey(sKey);
	string cipherKey = readCphrF(cipherKeyF);
	string recoveredKey = rsa_dec(cipherKey);
	if(setKey(recoveredKey))
	{
		cout << "ERROR loading recovered key" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////Decoding
	string cipher = readCphrF(cipherF);
	string recovered = dec(cipher);
	if(createF(recovered, outFilename))
	{
		cout << "ERROR creating decoded file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

bool serpent_ofb::KeysGenerator()
{
	if(genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	saveKeyIV("key", "iv");
	if(save_pubKey("pKey"))
	{
		return EXIT_FAILURE;
	}
	if(save_privKey("sKey"))
	{
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

serpent_ofb::~serpent_ofb(){}

/***************************************************************************/

serpent_cfb::serpent_cfb() : rsa(SERPENT, CFB) {}

string serpent_cfb::enc(string plain)
{
	string cipher;
	    
	try
	{	
		CFB_Mode< Serpent >::Encryption e;
		
		cout << e.AlgorithmName() << " encryption" << endl;
		
		e.SetKeyWithIV( key, keylength, iv );

		StringSource( plain, true, 
			new StreamTransformationFilter( e,
				new StringSink( cipher )
			) // StreamTransformationFilter      
		); // StringSource		
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

	return cipher;
}

bool serpent_cfb::encryption(const string& pKey, const string& key, const string& iv, const string& plainF)
{
	if(loadKeyIV(key, iv))
	{
		return EXIT_FAILURE;
	}			     
	
	//////////////////////////Encoding
	string plain, cipher;
	plain = readF(plainF);
	cipher = enc(plain);
	if(createCphrF(cipher, outFilename))
	{
		cout << "ERROR creating encoded file" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////RSA encoding
	load_pubKey(pKey);
	string cipherKey1 = rsa_enc(getKey());
	if(createF(cipherKey1, "cipherKey"))
	{
		cout << "ERROR creating encoded key file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

string serpent_cfb::dec(string cipher)
{
	string recovered;
	
	try
	{		
		CFB_Mode< Serpent >::Decryption d;
		
		cout << d.AlgorithmName() << " decryption" << endl;
		
		d.SetKeyWithIV( key, keylength, iv );

		StringSource( cipher, true, 
			new StreamTransformationFilter( d,
				new StringSink( recovered )
			) // StreamTransformationFilter
		); // StringSource
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

    return recovered;
}

bool serpent_cfb::decryption(const string& sKey, const string& cipherKeyF, const string& iv, const string& cipherF)
{
	if(loadIV(iv))
	{
		cout << "ERROR setting IV" << endl;
		return EXIT_FAILURE;
	}
	//////////////////////////RSA decoding
	load_privKey(sKey);
	string cipherKey = readCphrF(cipherKeyF);
	string recoveredKey = rsa_dec(cipherKey);
	if(setKey(recoveredKey))
	{
		cout << "ERROR loading recovered key" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////Decoding
	string cipher = readCphrF(cipherF);
	string recovered = dec(cipher);
	if(createF(recovered, outFilename))
	{
		cout << "ERROR creating decoded file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

bool serpent_cfb::KeysGenerator()
{
	if(genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	saveKeyIV("key", "iv");
	if(save_pubKey("pKey"))
	{
		return EXIT_FAILURE;
	}
	if(save_privKey("sKey"))
	{
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

serpent_cfb::~serpent_cfb(){}

/***************************************************************************/

serpent_ecb::serpent_ecb() : rsa(SERPENT, ECB) {}

string serpent_ecb::enc(string plain)
{
	string cipher;
	    
	try
	{	
		ECB_Mode< Serpent >::Encryption e;
		
		cout << e.AlgorithmName() << " encryption" << endl;
		
		e.SetKey( key, keylength );

		StringSource( plain, true, 
			new StreamTransformationFilter( e,
				new StringSink( cipher )
			) // StreamTransformationFilter      
		); // StringSource		
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

	return cipher;
}

bool serpent_ecb::encryption(const string& pKey, const string& key, const string& plainF)
{
	if(loadKey(key))
	{
		return EXIT_FAILURE;
	}			     
	
	//////////////////////////Encoding
	string plain, cipher;
	plain = readF(plainF);
	cipher = enc(plain);
	if(createCphrF(cipher, outFilename))
	{
		cout << "ERROR creating encoded file" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////RSA encoding
	load_pubKey(pKey);
	string cipherKey1 = rsa_enc(getKey());
	if(createF(cipherKey1, "cipherKey"))
	{
		cout << "ERROR creating encoded key file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

string serpent_ecb::dec(string cipher)
{
	string recovered;
	
	try
	{		
		ECB_Mode< Serpent >::Decryption d;
		
		cout << d.AlgorithmName() << " decryption" << endl;
		
		d.SetKey( key, keylength );

		StringSource( cipher, true, 
			new StreamTransformationFilter( d,
				new StringSink( recovered )
			) // StreamTransformationFilter
		); // StringSource
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

    return recovered;
}

bool serpent_ecb::decryption(const string& sKey, const string& cipherKeyF, const string& cipherF)
{
	//////////////////////////RSA decoding
	load_privKey(sKey);
	string cipherKey = readCphrF(cipherKeyF);
	string recoveredKey = rsa_dec(cipherKey);
	if(setKey(recoveredKey))
	{
		cout << "ERROR loading recovered key" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////Decoding
	string cipher = readCphrF(cipherF);
	string recovered = dec(cipher);
	if(createF(recovered, outFilename))
	{
		cout << "ERROR creating decoded file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

bool serpent_ecb::KeysGenerator()
{
	if(genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	saveKeyIV("key", "iv");
	if(save_pubKey("pKey"))
	{
		return EXIT_FAILURE;
	}
	if(save_privKey("sKey"))
	{
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

serpent_ecb::~serpent_ecb(){}

/***************************************************************************/

serpent_ctr::serpent_ctr() : rsa(SERPENT, CTR) {}

string serpent_ctr::enc(string plain)
{
	string cipher;
	    
	try
	{	
		CTR_Mode< Serpent >::Encryption e;
		
		cout << e.AlgorithmName() << " encryption" << endl;
		
		e.SetKeyWithIV( key, keylength, iv );

		StringSource( plain, true, 
			new StreamTransformationFilter( e,
				new StringSink( cipher )
			) // StreamTransformationFilter      
		); // StringSource		
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

	return cipher;
}

bool serpent_ctr::encryption(const string& pKey, const string& key, const string& iv, const string& plainF)
{
	if(loadKeyIV(key, iv))
	{
		return EXIT_FAILURE;
	}			     
	
	//////////////////////////Encoding
	string plain, cipher;
	plain = readF(plainF);
	cipher = enc(plain);
	if(createCphrF(cipher, outFilename))
	{
		cout << "ERROR creating encoded file" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////RSA encoding
	load_pubKey(pKey);
	string cipherKey1 = rsa_enc(getKey());
	if(createF(cipherKey1, "cipherKey"))
	{
		cout << "ERROR creating encoded key file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

string serpent_ctr::dec(string cipher)
{
	string recovered;
	
	try
	{		
		CTR_Mode< Serpent >::Decryption d;
		
		cout << d.AlgorithmName() << " decryption" << endl;
		
		d.SetKeyWithIV( key, keylength, iv );

		StringSource( cipher, true, 
			new StreamTransformationFilter( d,
				new StringSink( recovered )
			) // StreamTransformationFilter
		); // StringSource
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

    return recovered;
}

bool serpent_ctr::decryption(const string& sKey, const string& cipherKeyF, const string& iv, const string& cipherF)
{
	if(loadIV(iv))
	{
		cout << "ERROR setting IV" << endl;
		return EXIT_FAILURE;
	}
	//////////////////////////RSA decoding
	load_privKey(sKey);
	string cipherKey = readCphrF(cipherKeyF);
	string recoveredKey = rsa_dec(cipherKey);
	if(setKey(recoveredKey))
	{
		cout << "ERROR loading recovered key" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////Decoding
	string cipher = readCphrF(cipherF);
	string recovered = dec(cipher);
	if(createF(recovered, outFilename))
	{
		cout << "ERROR creating decoded file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

bool serpent_ctr::KeysGenerator()
{
	if(genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	saveKeyIV("key", "iv");
	if(save_pubKey("pKey"))
	{
		return EXIT_FAILURE;
	}
	if(save_privKey("sKey"))
	{
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

serpent_ctr::~serpent_ctr(){}
