//***********************************************************************
// Vincent COUVERT
// Florent LEBEAU
// DIGITEO 2009
// SciCrypt Toolbox
// This file is released under the terms of the CeCILL license.
//***********************************************************************

#include "tdes.hpp"


tdes_cbc::tdes_cbc() : rsa(TDES, CBC) {}

string tdes_cbc::enc(string plain)
{
	string cipher;

	try
	{
		CBC_Mode< DES_EDE3 >::Encryption e;

		cout << e.AlgorithmName() << " encryption" << endl;

		e.SetKeyWithIV( key, keylength, iv );

		StringSource( plain, true,
			new StreamTransformationFilter( e,
				new StringSink( cipher )
			) // StreamTransformationFilter
		); // StringSource
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

	return cipher;
}

bool tdes_cbc::encryption(const string& pKey, const string& key, const string& iv, const string& plainF)
{
	if(loadKeyIV(key, iv))
	{
		return EXIT_FAILURE;
	}

	//////////////////////////Encoding
	string plain, cipher;
	plain = readF(plainF);
	cipher = enc(plain);
	if(createCphrF(cipher, outFilename))
	{
		cout << "ERROR creating encoded file" << endl;
		return EXIT_FAILURE;
	}

	//////////////////////////RSA encoding
	load_pubKey(pKey);
	string cipherKey1 = rsa_enc(getKey());
	if(createF(cipherKey1, "cipherKey"))
	{
		cout << "ERROR creating encoded key file" << endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

string tdes_cbc::dec(string cipher)
{
	string recovered;

	try
	{
		CBC_Mode< DES_EDE3 >::Decryption d;

		cout << d.AlgorithmName() << " decryption" << endl;

	d.SetKeyWithIV( key, keylength, iv );

	StringSource( cipher, true,
		new StreamTransformationFilter( d,
			new StringSink( recovered )
		) // StreamTransformationFilter
	); // StringSource
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

    return recovered;
}

bool tdes_cbc::decryption(const string& sKey, const string& cipherKeyF, const string& iv, const string& cipherF)
{
	if(loadIV(iv))
	{
		cout << "ERROR setting IV" << endl;
		return EXIT_FAILURE;
	}
	//////////////////////////RSA decoding
	load_privKey(sKey);
	string cipherKey = readCphrF(cipherKeyF);
	string recoveredKey = rsa_dec(cipherKey);
	if(setKey(recoveredKey))
	{
		cout << "ERROR loading recovered key" << endl;
		return EXIT_FAILURE;
	}

	//////////////////////////Decoding
	string cipher = readCphrF(cipherF);
	string recovered = dec(cipher);
	if(createF(recovered, outFilename))
	{
		cout << "ERROR creating decoded file" << endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

bool tdes_cbc::KeysGenerator()
{
	if(genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	saveKeyIV("key", "iv");
	if(save_pubKey("pKey"))
	{
		return EXIT_FAILURE;
	}
	if(save_privKey("sKey"))
	{
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

tdes_cbc::~tdes_cbc(){}

/***************************************************************************/

tdes_ofb::tdes_ofb() : rsa(TDES, OFB) {}

string tdes_ofb::enc(string plain)
{
	string cipher;

	try
	{
		OFB_Mode< DES_EDE3 >::Encryption e;

		cout << e.AlgorithmName() << " encryption" << endl;

		e.SetKeyWithIV( key, keylength, iv );

		StringSource( plain, true,
			new StreamTransformationFilter( e,
				new StringSink( cipher )
			) // StreamTransformationFilter
		); // StringSource
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

	return cipher;
}

bool tdes_ofb::encryption(const string& pKey, const string& key, const string& iv, const string& plainF)
{
	if(loadKeyIV(key, iv))
	{
		return EXIT_FAILURE;
	}

	//////////////////////////Encoding
	string plain, cipher;
	plain = readF(plainF);
	cipher = enc(plain);
	if(createCphrF(cipher, outFilename))
	{
		cout << "ERROR creating encoded file" << endl;
		return EXIT_FAILURE;
	}

	//////////////////////////RSA encoding
	load_pubKey(pKey);
	string cipherKey1 = rsa_enc(getKey());
	if(createF(cipherKey1, "cipherKey"))
	{
		cout << "ERROR creating encoded key file" << endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

string tdes_ofb::dec(string cipher)
{
	string recovered;

	try
	{
		OFB_Mode< DES_EDE3 >::Decryption d;

		cout << d.AlgorithmName() << " decryption" << endl;

		d.SetKeyWithIV( key, keylength, iv );

		StringSource( cipher, true,
			new StreamTransformationFilter( d,
				new StringSink( recovered )
			) // StreamTransformationFilter
		); // StringSource
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

    return recovered;
}

bool tdes_ofb::decryption(const string& sKey, const string& cipherKeyF, const string& iv, const string& cipherF)
{
	if(loadIV(iv))
	{
		cout << "ERROR setting IV" << endl;
		return EXIT_FAILURE;
	}
	//////////////////////////RSA decoding
	load_privKey(sKey);
	string cipherKey = readCphrF(cipherKeyF);
	string recoveredKey = rsa_dec(cipherKey);
	if(setKey(recoveredKey))
	{
		cout << "ERROR loading recovered key" << endl;
		return EXIT_FAILURE;
	}

	//////////////////////////Decoding
	string cipher = readCphrF(cipherF);
	string recovered = dec(cipher);
	if(createF(recovered, outFilename))
	{
		cout << "ERROR creating decoded file" << endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

bool tdes_ofb::KeysGenerator()
{
	if(genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	saveKeyIV("key", "iv");
	if(save_pubKey("pKey"))
	{
		return EXIT_FAILURE;
	}
	if(save_privKey("sKey"))
	{
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

tdes_ofb::~tdes_ofb(){}

/***************************************************************************/

tdes_cfb::tdes_cfb() : rsa(TDES, CFB) {}

string tdes_cfb::enc(string plain)
{
	string cipher;

	try
	{
		CFB_Mode< DES_EDE3 >::Encryption e;

		cout << e.AlgorithmName() << " encryption" << endl;

		e.SetKeyWithIV( key, keylength, iv );

		StringSource( plain, true,
		    new StreamTransformationFilter( e,
			new StringSink( cipher )
		    ) // StreamTransformationFilter
		); // StringSource
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

	return cipher;
}

bool tdes_cfb::encryption(const string& pKey, const string& key, const string& iv, const string& plainF)
{
	if(loadKeyIV(key, iv))
	{
		return EXIT_FAILURE;
	}

	//////////////////////////Encoding
	string plain, cipher;
	plain = readF(plainF);
	cipher = enc(plain);
	if(createCphrF(cipher, outFilename))
	{
		cout << "ERROR creating encoded file" << endl;
		return EXIT_FAILURE;
	}

	//////////////////////////RSA encoding
	load_pubKey(pKey);
	string cipherKey1 = rsa_enc(getKey());
	if(createF(cipherKey1, "cipherKey"))
	{
		cout << "ERROR creating encoded key file" << endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

string tdes_cfb::dec(string cipher)
{
	string recovered;

	try
	{
		CFB_Mode< DES_EDE3 >::Decryption d;

		cout << d.AlgorithmName() << " decryption" << endl;

	d.SetKeyWithIV( key, keylength, iv );

	StringSource( cipher, true,
		new StreamTransformationFilter( d,
			new StringSink( recovered )
		) // StreamTransformationFilter
	); // StringSource
    	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

    return recovered;
}

bool tdes_cfb::decryption(const string& sKey, const string& cipherKeyF, const string& iv, const string& cipherF)
{
	if(loadIV(iv))
	{
		cout << "ERROR setting IV" << endl;
		return EXIT_FAILURE;
	}
	//////////////////////////RSA decoding
	load_privKey(sKey);
	string cipherKey = readCphrF(cipherKeyF);
	string recoveredKey = rsa_dec(cipherKey);
	if(setKey(recoveredKey))
	{
		cout << "ERROR loading recovered key" << endl;
		return EXIT_FAILURE;
	}

	//////////////////////////Decoding
	string cipher = readCphrF(cipherF);
	string recovered = dec(cipher);
	if(createF(recovered, outFilename))
	{
		cout << "ERROR creating decoded file" << endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

bool tdes_cfb::KeysGenerator()
{
	if(genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	saveKeyIV("key", "iv");
	if(save_pubKey("pKey"))
	{
		return EXIT_FAILURE;
	}
	if(save_privKey("sKey"))
	{
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

tdes_cfb::~tdes_cfb(){}

/***************************************************************************/

tdes_ecb::tdes_ecb() : rsa(TDES, ECB) {}

string tdes_ecb::enc(string plain)
{
	string cipher;

	try
	{
		ECB_Mode< DES_EDE3 >::Encryption e;

		cout << e.AlgorithmName() << " encryption" << endl;

		e.SetKey( key, keylength );

		StringSource( plain, true,
			new StreamTransformationFilter( e,
				new StringSink( cipher )
			) // StreamTransformationFilter
		); // StringSource
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

	return cipher;
}

bool tdes_ecb::encryption(const string& pKey, const string& key, const string& plainF)
{
	if(loadKey(key))
	{
		return EXIT_FAILURE;
	}

	//////////////////////////Encoding
	string plain, cipher;
	plain = readF(plainF);
	cipher = enc(plain);
	if(createCphrF(cipher, outFilename))
	{
		cout << "ERROR creating encoded file" << endl;
		return EXIT_FAILURE;
	}

	//////////////////////////RSA encoding
	load_pubKey(pKey);
	string cipherKey1 = rsa_enc(getKey());
	if(createF(cipherKey1, "cipherKey"))
	{
		cout << "ERROR creating encoded key file" << endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

string tdes_ecb::dec(string cipher)
{
	string recovered;

	try
	{
		ECB_Mode< DES_EDE3 >::Decryption d;

		cout << d.AlgorithmName() << " decryption" << endl;

		d.SetKey( key, keylength );

		StringSource( cipher, true,
			new StreamTransformationFilter( d,
				new StringSink( recovered )
			) // StreamTransformationFilter
		); // StringSource
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

    return recovered;
}

bool tdes_ecb::decryption(const string& sKey, const string& cipherKeyF, const string& cipherF)
{
	//////////////////////////RSA decoding
	load_privKey(sKey);
	string cipherKey = readCphrF(cipherKeyF);
	string recoveredKey = rsa_dec(cipherKey);
	if(setKey(recoveredKey))
	{
		cout << "ERROR loading recovered key" << endl;
		return EXIT_FAILURE;
	}

	//////////////////////////Decoding
	string cipher = readCphrF(cipherF);
	string recovered = dec(cipher);
	if(createF(recovered, outFilename))
	{
		cout << "ERROR creating decoded file" << endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

bool tdes_ecb::KeysGenerator()
{
	if(genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	saveKeyIV("key", "iv");
	if(save_pubKey("pKey"))
	{
		return EXIT_FAILURE;
	}
	if(save_privKey("sKey"))
	{
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

tdes_ecb::~tdes_ecb(){}

/***************************************************************************/

tdes_ctr::tdes_ctr() : rsa(TDES, CTR) {}

string tdes_ctr::enc(string plain)
{
	string cipher;

	try
	{
		CTR_Mode< DES_EDE3 >::Encryption e;

		cout << e.AlgorithmName() << " encryption" << endl;

		e.SetKeyWithIV( key, keylength, iv );

		StringSource( plain, true,
			new StreamTransformationFilter( e,
				new StringSink( cipher )
			) // StreamTransformationFilter
		); // StringSource
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

	return cipher;
}

bool tdes_ctr::encryption(const string& pKey, const string& key, const string& iv, const string& plainF)
{
	if(loadKeyIV(key, iv))
	{
		return EXIT_FAILURE;
	}

	//////////////////////////Encoding
	string plain, cipher;
	plain = readF(plainF);
	cipher = enc(plain);
	if(createCphrF(cipher, outFilename))
	{
		cout << "ERROR creating encoded file" << endl;
		return EXIT_FAILURE;
	}

	//////////////////////////RSA encoding
	load_pubKey(pKey);
	string cipherKey1 = rsa_enc(getKey());
	if(createF(cipherKey1, "cipherKey"))
	{
		cout << "ERROR creating encoded key file" << endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

string tdes_ctr::dec(string cipher)
{
	string recovered;

	try
	{
		CTR_Mode< DES_EDE3 >::Decryption d;

		cout << d.AlgorithmName() << " decryption" << endl;

		d.SetKeyWithIV( key, keylength, iv );

		StringSource( cipher, true,
			new StreamTransformationFilter( d,
				new StringSink( recovered )
			) // StreamTransformationFilter
		); // StringSource
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

    return recovered;
}

bool tdes_ctr::decryption(const string& sKey, const string& cipherKeyF, const string& iv, const string& cipherF)
{
	if(loadIV(iv))
	{
		cout << "ERROR setting IV" << endl;
		return EXIT_FAILURE;
	}
	//////////////////////////RSA decoding
	load_privKey(sKey);
	string cipherKey = readCphrF(cipherKeyF);
	string recoveredKey = rsa_dec(cipherKey);
	if(setKey(recoveredKey))
	{
		cout << "ERROR loading recovered key" << endl;
		return EXIT_FAILURE;
	}

	//////////////////////////Decoding
	string cipher = readCphrF(cipherF);
	string recovered = dec(cipher);
	if(createF(recovered, outFilename))
	{
		cout << "ERROR creating decoded file" << endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

bool tdes_ctr::KeysGenerator()
{
	if(genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	saveKeyIV("key", "iv");
	if(save_pubKey("pKey"))
	{
		return EXIT_FAILURE;
	}
	if(save_privKey("sKey"))
	{
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

tdes_ctr::~tdes_ctr(){}
