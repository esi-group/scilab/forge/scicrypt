//***********************************************************************
// Vincent COUVERT							
// Florent LEBEAU							
// DIGITEO 2009								
// SciCrypt Toolbox							
// This file is released under the terms of the CeCILL license.		
//***********************************************************************

tbx_build_gateway(..
	'SciCrypt_gtwy',..
	['encryptFile_aes', 'sci_encryptFile_aes'; 'decryptFile_aes', 'sci_decryptFile_aes'; 'keyGen_aes', 'sci_keyGen_aes';..
	'encryptFile_tdes', 'sci_encryptFile_tdes'; 'decryptFile_tdes', 'sci_decryptFile_tdes'; 'keyGen_tdes', 'sci_keyGen_tdes';..
	'encryptFile_serpent', 'sci_encryptFile_serpent'; 'decryptFile_serpent', 'sci_decryptFile_serpent'; 'keyGen_serpent', 'sci_keyGen_serpent';..
	'encryptFile_twofish', 'sci_encryptFile_twofish'; 'decryptFile_twofish', 'sci_decryptFile_twofish'; 'keyGen_twofish', 'sci_keyGen_twofish';..
	'encryptFile_cast256', 'sci_encryptFile_cast256'; 'decryptFile_cast256', 'sci_decryptFile_cast256'; 'keyGen_cast256', 'sci_keyGen_cast256';..
	'encryptFile_blowfish', 'sci_encryptFile_blowfish'; 'decryptFile_blowfish', 'sci_decryptFile_blowfish'; 'keyGen_blowfish', 'sci_keyGen_blowfish'],..
	['sci_encryptFile_aes.cxx', 'sci_decryptFile_aes.cxx', 'sci_keyGen_aes.cxx',..
	'sci_encryptFile_tdes.cxx', 'sci_decryptFile_tdes.cxx', 'sci_keyGen_tdes.cxx',..
	'sci_encryptFile_serpent.cxx', 'sci_decryptFile_serpent.cxx', 'sci_keyGen_serpent.cxx',..
	'sci_encryptFile_twofish.cxx', 'sci_decryptFile_twofish.cxx', 'sci_keyGen_twofish.cxx',..
	'sci_encryptFile_cast256.cxx', 'sci_decryptFile_cast256.cxx', 'sci_keyGen_cast256.cxx',..
	'sci_encryptFile_blowfish.cxx', 'sci_decryptFile_blowfish.cxx', 'sci_keyGen_blowfish.cxx'],..
	get_absolute_file_path('builder_gateway_cpp.sce'), ..
	['../../src/cpp/libSciCrypt_src'],..
	'',..
	'-I'+get_absolute_file_path('builder_gateway_cpp.sce')+'../../includes/ -I' + get_absolute_file_path('builder_gateway_cpp.sce') ..
	);

clear tbx_build_gateway;

