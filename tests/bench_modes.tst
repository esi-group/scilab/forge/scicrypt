mode(-1)
cd ../samples
////////////////////////////////// AES ECB
keyGen_aes("sampleKey", "sampleIV", "samplePublicKey", "samplePrivateKey")
nb_run = 100;
////// Encryption
timer();
for i = 1:nb_run
encryptFile_aes("sampleMatrix.sci", "samplePublicKey", "sampleKey", "0", "sampleIV", "cipher")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
ecbE = returned_time
////// Decryption
timer();
for i = 1:nb_run
decryptFile_aes("cipher", "samplePrivateKey", "cipherKey", "0", "sampleIV", "recovered")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
ecbD = returned_time
////////////////////////////////// AES CBC
////// Encryption
timer();
for i = 1:nb_run
encryptFile_aes("sampleMatrix.sci", "samplePublicKey", "sampleKey", "0", "sampleIV", "cipher")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
cbcE = returned_time
////// Decryption
timer();
for i = 1:nb_run
decryptFile_aes("cipher", "samplePrivateKey", "cipherKey", "0", "sampleIV", "recovered")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
cbcD = returned_time
////////////////////////////////// AES OFB
////// Encryption
timer();
for i = 1:nb_run
encryptFile_aes("sampleMatrix.sci", "samplePublicKey", "sampleKey", "0", "sampleIV", "cipher")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
ofbE = returned_time
////// Decryption
timer();
for i = 1:nb_run
decryptFile_aes("cipher", "samplePrivateKey", "cipherKey", "0", "sampleIV", "recovered")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
ofbD = returned_time
////////////////////////////////// AES CFB
////// Encryption
timer();
for i = 1:nb_run
encryptFile_aes("sampleMatrix.sci", "samplePublicKey", "sampleKey", "0", "sampleIV", "cipher")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
cfbE = returned_time
////// Decryption
timer();
for i = 1:nb_run
decryptFile_aes("cipher", "samplePrivateKey", "cipherKey", "0", "sampleIV", "recovered")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
cfbD = returned_time
////////////////////////////////// AES CTR
////// Encryption
timer();
for i = 1:nb_run
encryptFile_aes("sampleMatrix.sci", "samplePublicKey", "sampleKey", "0", "sampleIV", "cipher")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
ctrE = returned_time
////// Decryption
timer();
for i = 1:nb_run
decryptFile_aes("cipher", "samplePrivateKey", "cipherKey", "0", "sampleIV", "recovered")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
ctrD = returned_time

deletefile("cipher");
deletefile("cipherKey");
deletefile("recovered");

////////////////////////////////// Results
disp("--- MODES BENCHMARK ---")
disp("Encryption results (CPU Time)");
disp("AES CBC:");
disp(cbcE)
disp("AES OFB:")
disp(ofbE)
disp("AES CFB:")
disp(cfbE)
disp("AES ECB:")
disp(ecbE)
disp("AES CTR:")
disp(ctrE)
disp("Decryption results (CPU Time)");
disp("AES CBC:");
disp(cbcD)
disp("AES OFB:")
disp(ofbD)
disp("AES CFB:")
disp(cfbD)
disp("AES ECB:")
disp(ecbD)
disp("AES CTR:")
disp(ctrD)
clf()
classes = linspace(1,5,5)
resE = [cbcE ofbE cfbE ecbE ctrE]
resD = [cbcD ofbD cfbD ecbD ctrD]
xtitle("Modes benchmark (AES cipher)", "1=CBC, 2=OFB, 3=CFB, 4=ECB, 5=CTR", "CPU Time")
bar(classes, resE, 'blue')
bar(classes, resD, 'green')
legends(["Encryption";"Decryption"],[[2;1],[3;1]], with_box=%f, opt=1, font_size=2 )
