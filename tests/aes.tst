mode(-1)
cd ../samples
keyGen_aes("sampleKey", "sampleIV", "samplePublicKey", "samplePrivateKey")
// ECB mode
encryptFile_aes("sampleMatrix.sci", "samplePublicKey", "sampleKey", "0", "sampleIV", "cipher")
decryptFile_aes("cipher", "samplePrivateKey", "cipherKey", "0", "sampleIV", "recovered")
out = 0;
if chkFiles("sampleMatrix.sci", "recovered")
	disp("aes ECB: Success");
	out = out+1;
else
	disp("aes ECB: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// CBC mode
encryptFile_aes("sampleMatrix.sci", "samplePublicKey", "sampleKey", "1", "sampleIV", "cipher")
decryptFile_aes("cipher", "samplePrivateKey", "cipherKey", "1", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("aes CBC: Success");
	out = out+1;
else
	disp("aes CBC: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// OFB mode
encryptFile_aes("sampleMatrix.sci", "samplePublicKey", "sampleKey", "2", "sampleIV", "cipher")
decryptFile_aes("cipher", "samplePrivateKey", "cipherKey", "2", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("aes OFB: Success");
	out = out+1;
else
	disp("aes OFB: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// CFB mode
encryptFile_aes("sampleMatrix.sci", "samplePublicKey", "sampleKey", "3", "sampleIV", "cipher")
decryptFile_aes("cipher", "samplePrivateKey", "cipherKey", "3", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("aes CFB: Success");
	out = out+1;
else
	disp("aes CFB: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// CTR mode
encryptFile_aes("sampleMatrix.sci", "samplePublicKey", "sampleKey", "4", "sampleIV", "cipher")
decryptFile_aes("cipher", "samplePrivateKey", "cipherKey", "4", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("aes CTR: Success");
	out = out+1;
else
	disp("aes CTR: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// Conclusions
if out == 5
	disp("aes test: Success");
	aes = 1;
else
	disp("aes test: FAILURE");
	aes = 0;
end
deletefile("cipher");
deletefile("cipherKey");
deletefile("recovered");
cd ../tests
