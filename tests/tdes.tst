mode(-1)
cd ../samples
keyGen_tdes("sampleKey", "sampleIV", "samplePublicKey", "samplePrivateKey")
// ECB mode
encryptFile_tdes("sampleMatrix.sci", "samplePublicKey", "sampleKey", "0", "sampleIV", "cipher")
decryptFile_tdes("cipher", "samplePrivateKey", "cipherKey", "0", "sampleIV", "recovered")
out = 0;
if chkFiles("sampleMatrix.sci", "recovered")
	disp("TDES ECB: Success");
	out = out+1;
else
	disp("TDES ECB: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// CBC mode
encryptFile_tdes("sampleMatrix.sci", "samplePublicKey", "sampleKey", "1", "sampleIV", "cipher")
decryptFile_tdes("cipher", "samplePrivateKey", "cipherKey", "1", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("TDES CBC: Success");
	out = out+1;
else
	disp("TDES CBC: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// OFB mode
encryptFile_tdes("sampleMatrix.sci", "samplePublicKey", "sampleKey", "2", "sampleIV", "cipher")
decryptFile_tdes("cipher", "samplePrivateKey", "cipherKey", "2", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("TDES OFB: Success");
	out = out+1;
else
	disp("TDES OFB: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// CFB mode
encryptFile_tdes("sampleMatrix.sci", "samplePublicKey", "sampleKey", "3", "sampleIV", "cipher")
decryptFile_tdes("cipher", "samplePrivateKey", "cipherKey", "3", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("TDES CFB: Success");
	out = out+1;
else
	disp("TDES CFB: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// CTR mode
encryptFile_tdes("sampleMatrix.sci", "samplePublicKey", "sampleKey", "4", "sampleIV", "cipher")
decryptFile_tdes("cipher", "samplePrivateKey", "cipherKey", "4", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("TDES CTR: Success");
	out = out+1;
else
	disp("TDES CTR: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// Conclusions
if out == 5
	disp("TDES test: Success");
	tdes = 1;
else
	disp("TDES test: FAILURE");
	tdes = 0;
end
deletefile("cipher");
deletefile("cipherKey");
deletefile("recovered");
cd ../tests
