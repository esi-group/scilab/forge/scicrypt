mode(-1)
exec aes.tst
exec tdes.tst
exec serpent.tst
exec twofish.tst
exec cast256.tst
exec blowfish.tst
if aes + tdes + serpent + twofish + cast256 + blowfish == 6
	disp("Full test: Success");
else
	disp("Full test: FAILURE");
end
