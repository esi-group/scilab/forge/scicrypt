mode(-1)
cd ../samples
////////////////////////////////// AES OFB
keyGen_aes("sampleKey", "sampleIV", "samplePublicKey", "samplePrivateKey")
nb_run = 100;
////// Encryption
timer();
for i = 1:nb_run
encryptFile_aes("sampleMatrix.sci", "samplePublicKey", "sampleKey", "2", "sampleIV", "cipher")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
aesE = returned_time
////// Decryption
timer();
for i = 1:nb_run
decryptFile_aes("cipher", "samplePrivateKey", "cipherKey", "2", "sampleIV", "recovered")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
aesD = returned_time
////////////////////////////////// TDES OFB
keyGen_tdes("sampleKey", "sampleIV", "samplePublicKey", "samplePrivateKey")
////// Encryption
timer();
for i = 1:nb_run
encryptFile_tdes("sampleMatrix.sci", "samplePublicKey", "sampleKey", "2", "sampleIV", "cipher")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
tdesE = returned_time
////// Decryption
timer();
for i = 1:nb_run
decryptFile_tdes("cipher", "samplePrivateKey", "cipherKey", "2", "sampleIV", "recovered")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
tdesD = returned_time
////////////////////////////////// SERPENT OFB
keyGen_serpent("sampleKey", "sampleIV", "samplePublicKey", "samplePrivateKey")
////// Encryption
timer();
for i = 1:nb_run
encryptFile_serpent("sampleMatrix.sci", "samplePublicKey", "sampleKey", "2", "sampleIV", "cipher")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
srptE = returned_time
////// Decryption
timer();
for i = 1:nb_run
decryptFile_serpent("cipher", "samplePrivateKey", "cipherKey", "2", "sampleIV", "recovered")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
srptD = returned_time
////////////////////////////////// TWOFISH OFB
keyGen_twofish("sampleKey", "sampleIV", "samplePublicKey", "samplePrivateKey")
////// Encryption
timer();
for i = 1:nb_run
encryptFile_twofish("sampleMatrix.sci", "samplePublicKey", "sampleKey", "2", "sampleIV", "cipher")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
twfshE = returned_time
////// Decryption
timer();
for i = 1:nb_run
decryptFile_twofish("cipher", "samplePrivateKey", "cipherKey", "2", "sampleIV", "recovered")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
twfshD = returned_time

////////////////////////////////// CAST256 OFB
keyGen_cast256("sampleKey", "sampleIV", "samplePublicKey", "samplePrivateKey")
////// Encryption
timer();
for i = 1:nb_run
encryptFile_cast256("sampleMatrix.sci", "samplePublicKey", "sampleKey", "2", "sampleIV", "cipher")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
castE = returned_time
////// Decryption
timer();
for i = 1:nb_run
decryptFile_cast256("cipher", "samplePrivateKey", "cipherKey", "2", "sampleIV", "recovered")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
castD = returned_time

////////////////////////////////// BLOWFISH OFB
keyGen_blowfish("sampleKey", "sampleIV", "samplePublicKey", "samplePrivateKey")
////// Encryption
timer();
for i = 1:nb_run
encryptFile_blowfish("sampleMatrix.sci", "samplePublicKey", "sampleKey", "2", "sampleIV", "cipher")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
blwfshE = returned_time
////// Decryption
timer();
for i = 1:nb_run
decryptFile_blowfish("cipher", "samplePrivateKey", "cipherKey", "2", "sampleIV", "recovered")
end
timing = timer();
returned_time = timing * 1000000 / nb_run;
blwfshD = returned_time

deletefile("cipher");
deletefile("cipherKey");
deletefile("recovered");

////////////////////////////////// Results
disp("--- CIPHERS BENCHMARKS ---")
disp("Encryption results (CPU Time)");
disp("AES OFB:");
disp(aesE)
disp("TDES OFB:")
disp(tdesE)
disp("SERPENT OFB:")
disp(srptE)
disp("TWOFISH OFB:")
disp(twfshE)
disp("CAST256 OFB:")
disp(castE)
disp("Blowfish OFB:")
disp(blwfshE)
disp("Decryption results (CPU Time)");
disp("AES OFB:");
disp(aesD)
disp("TDES OFB:")
disp(tdesD)
disp("SERPENT OFB:")
disp(srptD)
disp("TWOFISH OFB:")
disp(twfshD)
disp("CAST256 OFB:")
disp(castD)
disp("Blowfish OFB:")
disp(blwfshD)
clf()
classes = linspace(1,6,6)
resE = [aesE tdesE srptE twfshE castE blwfshE]
resD = [aesD tdesD srptD twfshD castD blwfshD]
xtitle("Ciphers benchmark (OFB mode)", "1=AES, 2=TDES, 3=Serpent, 4=Twofish, 5=CAST256, 6=Blowfish", "CPU Time")
bar(classes, resE, 'blue')
bar(classes, resD, 'green')
legends(["Encryption";"Decryption"],[[2;1],[3;1]], with_box=%f, opt=1, font_size=2 )

