//***********************************************************************
// Vincent COUVERT
// Florent LEBEAU
// DIGITEO 2009
// SciCrypt Toolbox
// This file is released under the terms of the CeCILL license.
//***********************************************************************

#ifndef _FUNCTIONS_HPP
#define _FUNCTIONS_HPP

/*************************** Include C++ ****************************/
#include <iostream>
using namespace std;

/************************* Include Crypto++ *************************/
#include "cryptopp/cryptlib.h"
using namespace CryptoPP;

#include "cryptopp/rsa.h"
using CryptoPP::RSA;
using CryptoPP::InvertibleRSAFunction;

/************************* Include SciCrypt *************************/
#include "aes.hpp"
#include "tdes.hpp"
#include "serpent.hpp"
#include "twofish.hpp"
#include "cast256.hpp"
#include "blowfish.hpp"

/********************************************************************/

////////////// AES
int encryptFile_aes(char* plainF, char* pKey, char* key, int m, char* iv, char* outFn);
int decryptFile_aes(char* cipherF, char* sKey, char* cipherKeyF, int m, char* iv, char* outFn);
int keyGen_aes(char* key, char* iv, char* pKey, char* sKey);
////////////// TDES
int encryptFile_tdes(char* plainF, char* pKey, char* key, int m, char* iv, char* outFn);
int decryptFile_tdes(char* cipherF, char* sKey, char* cipherKeyF, int m, char* iv, char* outFn);
int keyGen_tdes(char* key, char* iv, char* pKey, char* sKey);
////////////// SERPENT
int encryptFile_serpent(char* plainF, char* pKey, char* key, int m, char* iv, char* outFn);
int decryptFile_serpent(char* cipherF, char* sKey, char* cipherKeyF, int m, char* iv, char* outFn);
int keyGen_serpent(char* key, char* iv, char* pKey, char* sKey);
////////////// TWOFISH
int encryptFile_twofish(char* plainF, char* pKey, char* key, int m, char* iv, char* outFn);
int decryptFile_twofish(char* cipherF, char* sKey, char* cipherKeyF, int m, char* iv, char* outFn);
int keyGen_twofish(char* key, char* iv, char* pKey, char* sKey);
////////////// CAST256
int encryptFile_cast256(char* plainF, char* pKey, char* key, int m, char* iv, char* outFn);
int decryptFile_cast256(char* cipherF, char* sKey, char* cipherKeyF, int m, char* iv, char* outFn);
int keyGen_cast256(char* key, char* iv, char* pKey, char* sKey);
////////////// BLOWFISH
int encryptFile_blowfish(char* plainF, char* pKey, char* key, int m, char* iv, char* outFn);
int decryptFile_blowfish(char* cipherF, char* sKey, char* cipherKeyF, int m, char* iv, char* outFn);
int keyGen_blowfish(char* key, char* iv, char* pKey, char* sKey);

#endif
