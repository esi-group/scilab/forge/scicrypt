//***********************************************************************
// Vincent COUVERT							
// Florent LEBEAU							
// DIGITEO 2009								
// SciCrypt Toolbox							
// This file is released under the terms of the CeCILL license.		
//***********************************************************************

#ifndef _AES_HPP
#define _AES_HPP

/*************************** Include C++ ****************************/
#include <iostream>
using namespace std;

/************************* Include Crypto++ *************************/
#include "cryptopp/osrng.h"
using CryptoPP::AutoSeededRandomPool;

#include "cryptopp/files.h"
using CryptoPP::FileSink;
using CryptoPP::FileSource;

#include "cryptopp/cryptlib.h"
using namespace CryptoPP;

#include "cryptopp/aes.h"
using CryptoPP::AES;

#include "cryptopp/modes.h"
using CryptoPP::OFB_Mode;
using CryptoPP::CBC_Mode;
using CryptoPP::CFB_Mode;

#include "cryptopp/rsa.h"
using CryptoPP::RSA;

/************************* Include SciCrypt *************************/
#include "rsa.hpp"

/********************************************************************/

class aes_cbc : public rsa
{
	public:
		aes_cbc();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~aes_cbc();
};

class aes_ofb : public rsa
{
	public:
		aes_ofb();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~aes_ofb();
};

class aes_cfb : public rsa
{
	public:
		aes_cfb();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~aes_cfb();
};

class aes_ecb : public rsa
{
	public:
		aes_ecb();
		string enc(string);
		bool encryption(const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&);
		bool KeysGenerator();
		~aes_ecb();
};

class aes_ctr : public rsa
{
	public:
		aes_ctr();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~aes_ctr();
};

#endif
