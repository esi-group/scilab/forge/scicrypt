function [ret] = chkFiles (file1, file2)
  cont1 = readc_(file1);
  cont2 = readc_(file2);
  if cont1 == cont2
    ret = 0;
  else 
    ret = 1;
  end
endfunction 
