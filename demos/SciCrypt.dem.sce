//***********************************************************************
// Vincent COUVERT							
// Florent LEBEAU							
// DIGITEO 2009								
// SciCrypt Toolbox							
// This file is released under the terms of the CeCILL license.		
//***********************************************************************
demos_dir = get_absolute_file_path('SciCrypt.dem.sce');
cd(demos_dir);
mode(-1)
createdir('Alice');
createdir('Bob');
disp("Alice and Bob would like to share secret files. They both agree on a couple of RSA keys.");
keyGen_aes("AliceKey", "AliceIV", "AlicePublicKey", "BobPrivateKey");
copyfile("AlicePublicKey","./Alice/AlicePublicKey");
copyfile("BobPrivateKey","./Bob/BobPrivateKey");
mode (1)
dir ./Alice
dir ./Bob
mode (-1)
realtimeinit(1)
realtime(0)
realtime(15)
copyfile("../samples/sampleMacro.sci","./Alice/AliceMacro.sci");
copyfile("AliceKey","./Alice/AliceKey");
copyfile("AliceIV","./Alice/AliceIV");
deletefile("AliceKey");
deletefile("AliceIV");
deletefile("AlicePublicKey");
deletefile("BobPrivateKey");
disp("Alice wants to cipher the file AliceMacro.sci with the Twofish OFB algorithm. She chooses a key and and an initialisation vector (IV).");
mode(1)
dir ./Alice
mode(-1)
realtimeinit(1)
realtime(0)
realtime(15)
cd ./Alice
mode(1)
encryptFile_twofish("AliceMacro.sci", "AlicePublicKey", "AliceKey","2", "AliceIV","cipher");
dir
mode(-1)
cd ../
realtimeinit(1)
realtime(0)
realtime(15)
disp("She sends the file cipher to Bob, which corresponds to the encrypted AliceMacro.sci. She also sends iv, and cipherKey, wich corresponds to the RSA encrypted key.");
copyfile("./Alice/cipher","./Bob/cipher");
copyfile("./Alice/cipherKey","./Bob/cipherKey");
copyfile("./Alice/AliceIV","./Bob/AliceIV");
mode(1)
dir ./Bob
mode(-1)
realtimeinit(1)
realtime(0)
realtime(15)
disp("Bob now decrypts the file cipher.");
cd ./Bob
mode(1)
decryptFile_twofish_ofb("cipher", "BobPrivateKey", "cipherKey", "AliceIV");
dir
mode(-1)
realtimeinit(1)
realtime(0)
realtime(15)
cd ../
copyfile("./Bob/recovered","./Bob/recovered.sci")
disp("Is the recovered file identical to AliceMacro.sci ?")
disp(chkFiles("./Alice/AliceMacro.sci","./Bob/recovered"))
realtimeinit(1)
realtime(0)
realtime(15)
scipad "./Alice/AliceMacro.sci" "./Bob/recovered.sci";
removedir('Alice')
removedir('Bob')
